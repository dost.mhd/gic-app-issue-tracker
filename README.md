# README #

Please Report bugs and problems here. you can even put additional issues here like something missing or enhancement requests. The setup is made at   [146.185.153.124](http://146.185.153.124) 

# Access #

System admin:
	Username :admin
	Pass:admin123

### Creating an issue? ###

* Open the [Link](https://bitbucket.org/dmsherazi1/gic-app-issue-tracker/issues)
* Add a new issue or open an existing one

### Tools you can use  ###

* You can use SNIPPING Tool that comes built-in with windows.It has tools to annotate and point out problems [TUTORIAL ](http://www.howtogeek.com/207754/how-to-use-the-snipping-tool-in-windows-to-take-screenshots/)
* If you are using Windows 10 Edge Browser has a nice feature you can use [TUTORIAL ](http://www.tech-recipes.com/rx/56213/how-to-annotate-webpages-with-microsoft-edge/)
* ...